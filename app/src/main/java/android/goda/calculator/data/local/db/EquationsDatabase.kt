package android.goda.calculator.data.local.db

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(
    entities = [EquationEntity::class],
    version = 3
)
abstract class EquationsDatabase : RoomDatabase(){
    abstract fun getEquationDAO(): EquationDAO
}
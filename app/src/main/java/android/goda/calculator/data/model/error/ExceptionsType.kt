package android.goda.calculator.data.model.error

import java.lang.RuntimeException
sealed class ExceptionsType{
    class UndefinedOperationException : RuntimeException("Operation is undefined.")
}

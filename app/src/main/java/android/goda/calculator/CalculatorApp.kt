package android.goda.calculator

import android.app.Application
import android.content.Context
import com.google.android.libraries.places.api.Places
import android.goda.calculator.di.ComponentProvider
import android.goda.calculator.di.application.AppComponent
import android.goda.calculator.di.application.AppModule
import android.goda.calculator.di.application.DaggerAppComponent
import timber.log.Timber

class CalculatorApp: Application(), ComponentProvider<AppComponent> {

    companion object{
        fun getComponent(context: Context): AppComponent {
            return getApp(context).getComponent()
        }

        private fun getApp(context: Context): CalculatorApp {
            return context.applicationContext as CalculatorApp
        }
    }


    private val appComponent: AppComponent = createAppComponent()

    private fun createAppComponent(): AppComponent = DaggerAppComponent.builder()
            .appModule(AppModule(this))
            .build()


    override fun onCreate() {
        super.onCreate()
        Places.initialize(this,getString(R.string.google_api_key))

        appComponent.inject(this)
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }

    override fun getComponent(): AppComponent = appComponent


}
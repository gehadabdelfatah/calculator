package android.goda.calculator.di.application

import dagger.Component
import android.goda.calculator.CalculatorApp
import android.goda.calculator.utils.alarm.AlarmBroadcastReceiver
import android.goda.calculator.di.activity.ActivityComponent
import android.goda.calculator.di.activity.ActivityModule
import android.goda.calculator.di.scope.ApplicationScope

@ApplicationScope
@Component(modules = [AppModule::class])
interface AppComponent {

    fun plus(activityModule: ActivityModule?): ActivityComponent?

    fun inject(calculator: CalculatorApp)
    fun inject(alarmBroadcastReceiver: AlarmBroadcastReceiver)
}
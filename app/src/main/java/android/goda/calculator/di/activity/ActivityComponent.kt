package android.goda.calculator.di.activity

import dagger.Subcomponent
import android.goda.calculator.di.fragment.FragmentComponent
import android.goda.calculator.di.fragment.FragmentModule
import android.goda.calculator.di.scope.ActivityScope
import android.goda.calculator.presentation.activity.MainActivity

/**
 * This interface is used by dagger to generate the code that defines the connection between the provider of objects
 * (i.e. [ActivityModule]), and the object which expresses a dependency.
 */
@ActivityScope
@Subcomponent(modules = [ActivityModule::class])
interface ActivityComponent {
    fun plus(fragmentModule: FragmentModule): FragmentComponent
    fun inject(mainActivity: MainActivity)
}
package android.goda.calculator.di.application

import android.app.Application
import android.content.Context
import androidx.lifecycle.ViewModelProvider
import androidx.room.Room
import dagger.Module
import dagger.Provides
import android.goda.calculator.data.local.db.EquationDAO
import android.goda.calculator.data.local.db.EquationsDatabase
import android.goda.calculator.di.scope.ApplicationScope
import android.goda.calculator.utils.operators.CalculatorEg
import android.goda.calculator.repository.RepositoryImpl

@Module
class AppModule(val application: Application) {

    @ApplicationScope
    @Provides
    fun providesApplication(): Application {
        return application
    }

    @ApplicationScope
    @Provides
    fun providesApplicationContext(): Context {
        return application
    }

    @ApplicationScope
    @Provides
    fun provideViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory = factory

    @ApplicationScope
    @Provides
    fun providesRoomDatabase(): EquationsDatabase =
        Room.databaseBuilder(application, EquationsDatabase::class.java, "equations_database")
            .fallbackToDestructiveMigration().build()


    @ApplicationScope
    @Provides
    fun providesEquationDAO(equationsDatabase: EquationsDatabase) = equationsDatabase.getEquationDAO()


    @ApplicationScope
    @Provides
    fun providesAppRepository(equationDAO: EquationDAO) = RepositoryImpl(equationDAO)

    @ApplicationScope
    @Provides
    fun provideAppCalcEngine() = CalculatorEg()


}
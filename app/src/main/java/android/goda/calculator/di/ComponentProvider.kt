package android.goda.calculator.di

fun interface ComponentProvider<T> {
    fun getComponent(): T
}
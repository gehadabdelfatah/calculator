package android.goda.calculator.di.fragment

import dagger.Subcomponent
import android.goda.calculator.di.scope.FragmentScope
import android.goda.calculator.presentation.fragments.calculator.CalculatorFragment

/**
 * This interface is used by dagger to generate the code that defines the connection between the provider of objects
 * (i.e. [FragmentModule]), and the object which expresses a dependency.
 */
@FragmentScope
@Subcomponent(modules = [FragmentModule::class])
interface FragmentComponent {
    fun inject(calculatorFragment: CalculatorFragment)
}
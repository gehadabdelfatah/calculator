package android.goda.calculator.di.application

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import android.goda.calculator.di.scope.ApplicationScope
import android.goda.calculator.utils.operators.CalculatorEg
import android.goda.calculator.repository.RepositoryImpl
import android.goda.calculator.presentation.viewmodel.CalculatorViewModel
import javax.inject.Inject

@ApplicationScope
class ViewModelFactory @Inject constructor(private var repositoryImpl: RepositoryImpl, private var eg: CalculatorEg) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return CalculatorViewModel(repositoryImpl, eg)  as T
    }
}
package android.goda.calculator.presentation.activity


import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupWithNavController
import kotlinx.android.synthetic.main.activity_main.*
import android.goda.calculator.R
import android.goda.calculator.CalculatorApp
import android.goda.calculator.di.ComponentProvider
import android.goda.calculator.di.activity.ActivityComponent
import android.goda.calculator.di.activity.ActivityModule
import android.goda.calculator.di.application.AppComponent

class MainActivity : AppCompatActivity(), ComponentProvider<ActivityComponent>{

    private var activityComponent: ActivityComponent? = null

    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initDagger()

       // val navHostFragment = supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment
        //val navController = navHostFragment.navController

        //bottom_navigation_view.setupWithNavController(navController)

    }
    private fun initDagger(){
        val appComponent: AppComponent = CalculatorApp.getComponent(applicationContext)
        activityComponent = appComponent.plus(ActivityModule(this))
        activityComponent!!.inject(this)
    }
    override fun getComponent(): ActivityComponent {

        if (activityComponent == null){
            val appComponent: AppComponent = CalculatorApp.getComponent(applicationContext)
            activityComponent = appComponent.plus(ActivityModule(this))
            activityComponent?.inject(this)
        }
        return activityComponent!!
    }

}
package android.goda.calculator.presentation.fragments.calculator

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Geocoder
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.children
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import android.goda.calculator.data.model.Resource
import android.goda.calculator.data.local.db.EquationEntity
import android.goda.calculator.databinding.FragmentMainBinding
import android.goda.calculator.di.ComponentProvider
import android.goda.calculator.di.activity.ActivityComponent
import android.goda.calculator.di.application.ViewModelFactory
import android.goda.calculator.di.fragment.FragmentModule
import android.goda.calculator.presentation.adapters.LogAdapter
import android.goda.calculator.utils.Constant.ACTION_START_SERVICE
import android.goda.calculator.utils.Constant.EQUATION_ID
import android.goda.calculator.utils.Constant.MATH_EQUATION_SCHEDULED_TIME

import android.goda.calculator.utils.service.MathCalculatorService
import android.goda.calculator.presentation.viewmodel.CalculatorViewModel
import android.location.Location
import android.location.LocationManager
import android.os.Looper
import android.provider.Settings
import androidx.core.app.ActivityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.gms.location.*
import com.google.android.gms.tasks.OnCompleteListener

import timber.log.Timber
import java.io.IOException
import java.util.*
import javax.inject.Inject

class CalculatorFragment : Fragment(), View.OnClickListener {
    private val mLocationCallback: LocationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            val mLastLocation = locationResult.lastLocation
            setAddress(mLastLocation)
        }
    }
    private lateinit var calculatorViewModel: CalculatorViewModel
    private var _binding: FragmentMainBinding? = null

    @Inject
    lateinit var factory: ViewModelFactory

    private lateinit var logAdapter: LogAdapter

    @Inject
    lateinit var geocoder: Geocoder
    private var timeToScheduled: Int = 0
    var mFusedLocationClient: FusedLocationProviderClient? = null
    var PERMISSION_ID = 44
    private val operands = listOf("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", ".")


    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is ComponentProvider<*>) {
            val provider = context as ComponentProvider<*>
            if (provider.getComponent() is ActivityComponent) {
                (provider.getComponent() as ActivityComponent)
                    .plus(FragmentModule(this))
                    .inject(this)
            } else {
                throw IllegalStateException("Component must be " + ActivityComponent::class.java.name)
            }
        } else {
            throw IllegalStateException("host context must implement " + ComponentProvider::class.java.name)
        }
    }

    private fun setupRV() {
        _binding?.let {
            with(it) {
                val layoutManager = LinearLayoutManager(requireContext())
                equationsListRV.layoutManager = layoutManager
                equationsListRV.setHasFixedSize(true)
                logAdapter = LogAdapter()
                equationsListRV.adapter = logAdapter
            }
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        initViewModel()
        requestPermissions()
        _binding = FragmentMainBinding.inflate(inflater, container, false)
        return _binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        registerButtonClick()
        observeOnInsertedID()
        setupRV()
        calculatorViewModel.getAllEquations()
        setSubscribeEquationsData()
        super.onViewCreated(view, savedInstanceState)
    }

    private fun setSubscribeEquationsData() {
        calculatorViewModel.allEquationLiveData.observe(viewLifecycleOwner) {
            it?.let {
                when (it) {
                    is Resource.Success -> it.data?.let { equationList -> bindListData(equationList) }
                    is Resource.DataError -> {
                        Timber.e(it.errorMassage)
                        showDataView(false)
                    }
                }
            }
        }
    }

    private fun initViewModel() {
        calculatorViewModel = ViewModelProvider(this, factory).get(CalculatorViewModel::class.java)
    }

    private fun bindListData(list: List<EquationEntity>) {
        if (!(list.isNullOrEmpty())) {
            logAdapter.setData(list)
            logAdapter.notifyDataSetChanged()
            showDataView(true)
        } else {
            showDataView(false)
        }
    }

    fun requestLoction() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(activity)
        getLastLocation()
    }

    private fun setAddress(location: Location) {
        val geocoder: Geocoder
        geocoder = Geocoder(activity, Locale.getDefault())
        try {
            val addresses = geocoder.getFromLocation(location.latitude, location.longitude, 1)
            val address =
                addresses[0].getAddressLine(0) // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            val city = addresses[0].locality
            val state = addresses[0].adminArea
            val country = addresses[0].countryName
            val postalCode = addresses[0].postalCode
            val knownName = addresses[0].featureName
            _binding?.tvLocation?.visibility = View.VISIBLE
            _binding?.tvLocation?.text =
                address // Here 1 represent max location result to returned, by documents it recommended 1 to 5
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    @SuppressLint("MissingPermission")
    private fun getLastLocation() {
        if (checkPermissions()) {
            if (isLocationEnabled()) {
                mFusedLocationClient?.lastLocation
                    ?.addOnCompleteListener(OnCompleteListener<Location?> { task ->
                        val location = task.result
                        if (location == null) {
                            requestNewLocationData()
                        } else {
                            setAddress(location)
                        }
                    })
            } else {
                Toast.makeText(context, "Please turn on" + " your location...", Toast.LENGTH_LONG)
                    .show()
                val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                startActivity(intent)
            }
        } else {
            requestPermissions()
        }
    }

    @SuppressLint("MissingPermission")
    private fun requestNewLocationData() {
        val mLocationRequest = LocationRequest()
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        mLocationRequest.interval = 5
        mLocationRequest.fastestInterval = 0
        mLocationRequest.numUpdates = 1
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(activity)
        mFusedLocationClient?.requestLocationUpdates(
            mLocationRequest,
            mLocationCallback,
            Looper.myLooper()
        )
    }

    private fun checkPermissions(): Boolean {
        return ActivityCompat.checkSelfPermission(
            requireContext(),
            Manifest.permission.ACCESS_COARSE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
            requireContext(),
            Manifest.permission.ACCESS_FINE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED
    }

    private fun requestPermissions() {
        ActivityCompat.requestPermissions(
            requireActivity(), arrayOf(
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION
            ), PERMISSION_ID
        )
    }

    private fun isLocationEnabled(): Boolean {
        val locationManager =
            requireContext().getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
            LocationManager.NETWORK_PROVIDER
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == PERMISSION_ID) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getLastLocation()
            }
        }
    }

    private fun showDataView(show: Boolean) {
        _binding?.equationsListRV?.visibility = if (show) {
            View.VISIBLE
        } else View.GONE
        _binding?.equationsListRV?.scrollToPosition((logAdapter.itemCount) - 1)
        if (true) {
            requestLoction()
        }
    }

    private fun registerButtonClick() {
        _binding?.let { fragmentMainBinding ->
            with(fragmentMainBinding) {
                (layoutButtonHolder as ViewGroup).children.forEach {
                    it.setOnClickListener(this@CalculatorFragment)
                }
            }
        }

    }

    override fun onClick(v: View?) {

        // Every button must have a tag
        if (v == null || v.tag == null) {
            return
        }
        when (val tag = v.tag.toString()) {
            "del" -> {
                // Delete last character from input
                val oldValue = readInputValue()
                if (oldValue.isNotEmpty()) {
                    _binding?.editTextInput?.setText(oldValue.substring(0, oldValue.lastIndex))
                }
            }
            "C" -> resetCalculator()
            in operands -> {
                // Handle numerical values
                if (readInputValue().contains(".") && tag == ".") {
                    return
                }

                val s = readInputValue() + tag
                _binding?.editTextInput?.setText(s)
            }
            else -> {

                if (tag != "=") {
                    _binding?.textOperator?.text = tag
                    if (readInputValue().isNotEmpty()) {
                        _binding?.textResult?.text = readInputValue()
                    }
                    _binding?.editTextInput?.setText("")
                } else {
                    if (readInputValue().isNotEmpty()) {
                        doCalculate()
                    }
                }

            }
        }
    }

    private fun resetCalculator() {
        _binding?.let {
            with(it) {
                editTextInput.setText("")
                textOperator.text = "+"
                textResult.text = "0"
            }
        }

        timeToScheduled = 0
    }

    private fun readInputValue() = _binding?.editTextInput?.text.toString()


    fun doCalculate() {
        _binding?.let {
            with(it) {
                val timeString = timeET.text.toString()
                timeToScheduled = if (timeString.isEmpty()) {
                    0
                } else {
                    timeString.toInt()
                }

                val result = calculatorViewModel.doCalculation(
                    textResult.text.toString().toFloat(),
                    textOperator.text.toString(), readInputValue().toFloat()
                )

                insertEquation(result)
            }
        }
    }

    private fun insertEquation(result: Resource<Float>) {
        when (result) {
            is Resource.Success -> {
                result.data?.let { equationResult ->
                    _binding?.let {
                        with(it) {
                            calculatorViewModel.insertEquation(
                                EquationEntity(
                                    textResult.text.toString().toFloat(),
                                    readInputValue().toFloat(), textOperator.text.toString(),
                                    equationResult, false
                                )
                            )
                        }
                    }
                }
            }
            is Resource.DataError -> {
                Toast.makeText(requireContext(), result.errorMassage, Toast.LENGTH_SHORT).show()
            }

        }

    }

    private fun observeOnInsertedID() {
        calculatorViewModel.insertedID.observe(viewLifecycleOwner) {
            if (it.peekContent() != -1L) {
                startMathEngineService(timeToScheduled, it?.peekContent())
            }
        }
    }

    private fun startMathEngineService(time: Int, equationID: Long?) =
        Intent(requireContext(), MathCalculatorService::class.java).also {
            it.action = ACTION_START_SERVICE
            it.putExtra(MATH_EQUATION_SCHEDULED_TIME, time)
            it.putExtra(EQUATION_ID, equationID)
            requireContext().startService(it)
            resetCalculator()
        }


}
package android.goda.calculator.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import android.goda.calculator.data.model.Resource
import android.goda.calculator.data.local.db.EquationEntity
import android.goda.calculator.utils.operators.CalculatorEg
import android.goda.calculator.utils.Constant.CANT_DIVIDE
import android.goda.calculator.utils.Constant.NUM_1_NULL_MSG
import android.goda.calculator.utils.Constant.NUM_2_NULL_MSG
import android.goda.calculator.utils.Constant.OPERATOR_NULL_OR_EMPTY_MSG
import android.goda.calculator.utils.SingleEvent
import android.goda.calculator.repository.CalculatorRepository
import javax.inject.Inject

class CalculatorViewModel @Inject constructor(private val calculatorRepository: CalculatorRepository,
                                              private val eg: CalculatorEg ) : ViewModel() {


    private val allEquationLiveDataPrivate = MutableLiveData<Resource<List<EquationEntity>>>()
    val allEquationLiveData: LiveData<Resource<List<EquationEntity>>> get() = allEquationLiveDataPrivate

    private val insertedIDPrivate = MutableLiveData<SingleEvent<Long>>()
    val insertedID: LiveData<SingleEvent<Long>> get() = insertedIDPrivate


    fun doCalculation(num1: Float?, operator: String?, num2: Float?) : Resource<Float> {
        if (operator.equals("/") && num1 == 0f){
            return Resource.DataError(CANT_DIVIDE)
        }
        when {
            num1 == null -> {
                return Resource.DataError(NUM_1_NULL_MSG)
            }
            num2 == null -> {
                return Resource.DataError(NUM_2_NULL_MSG)
            }
            operator.isNullOrEmpty() -> {
                return Resource.DataError(OPERATOR_NULL_OR_EMPTY_MSG)
            }
            else -> {
                eg.operator = operator
                val result = eg.calculate(num1, num2)
                eg.clear()
                return Resource.Success(result)
            }
        }

    }

    fun insertEquation(equation: EquationEntity){
        viewModelScope.launch {
            insertedIDPrivate.value = SingleEvent(calculatorRepository.insertEquation(equation))
        }
    }

    fun getAllEquations(){
        viewModelScope.launch {
            try {
                calculatorRepository.getAllEquations().collect {
                    allEquationLiveDataPrivate.value = Resource.Success(it)
                }
            } catch (e: Exception) {
                allEquationLiveDataPrivate.value = Resource.DataError(e.toString())
            }
        }
    }
}
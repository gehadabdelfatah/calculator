
package android.goda.calculator.utils.operators


object AddOperator : Calculate {
  override fun apply(leftOperand: Float, rightOperand: Float): Float = leftOperand.plus(rightOperand)
}
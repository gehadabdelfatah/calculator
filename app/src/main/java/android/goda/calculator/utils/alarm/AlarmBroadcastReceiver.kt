package android.goda.calculator.utils.alarm

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import android.goda.calculator.CalculatorApp
import android.goda.calculator.utils.Constant.EQUATION_ID
import android.goda.calculator.repository.RepositoryImpl
import javax.inject.Inject

class AlarmBroadcastReceiver : BroadcastReceiver() {

    @Inject
    lateinit var repositoryImpl: RepositoryImpl

    override fun onReceive(context: Context?, intent: Intent?) {
        CalculatorApp.getComponent(context!!).inject(this)

        intent?.let {
            if (it.hasExtra(EQUATION_ID)) {

                val equationID =  it.getLongExtra(EQUATION_ID, 0L)

                CoroutineScope(Dispatchers.IO).launch {
                    repositoryImpl.updateEquation(equationID)
                }
            }
        }
    }
}
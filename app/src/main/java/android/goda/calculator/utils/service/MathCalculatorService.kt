package android.goda.calculator.utils.service

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import androidx.lifecycle.LifecycleService
import android.goda.calculator.utils.alarm.AlarmBroadcastReceiver
import android.goda.calculator.utils.Constant.ACTION_START_SERVICE
import android.goda.calculator.utils.Constant.EQUATION_ID
import android.goda.calculator.utils.Constant.MATH_EQUATION_SCHEDULED_TIME
import java.util.*

class MathCalculatorService : LifecycleService() {


    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {

        intent?.let {
            when (it.action) {
                ACTION_START_SERVICE -> {
                    if (it.hasExtra(MATH_EQUATION_SCHEDULED_TIME)&&
                        it.hasExtra(EQUATION_ID)){
                        createScheduledAlarm(it)
                    }
                }
            }
        }
        return super.onStartCommand(intent, flags, startId)
    }


    private fun createScheduledAlarm(it: Intent) {

        val equationID = it.getLongExtra(EQUATION_ID, 0L)

        val time = it.getIntExtra(MATH_EQUATION_SCHEDULED_TIME, 0)

        val alarmManager = getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val intent = Intent(this, AlarmBroadcastReceiver::class.java)
            intent.putExtra(EQUATION_ID, equationID)

        val alarmPendingIntent = PendingIntent.getBroadcast(this, equationID.toInt(), intent, PendingIntent.FLAG_UPDATE_CURRENT)

        val calendar = Calendar.getInstance()
        calendar.add(Calendar.SECOND, time)

        alarmManager.setExact(AlarmManager.RTC_WAKEUP, calendar.timeInMillis, alarmPendingIntent)
    }

}



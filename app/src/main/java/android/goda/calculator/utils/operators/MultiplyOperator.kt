
package android.goda.calculator.utils.operators

object MultiplyOperator : Calculate {
  override fun apply(leftOperand: Float, rightOperand: Float): Float = leftOperand.times(rightOperand)
}

package android.goda.calculator.utils.operators


interface Calculate {
  /**
   * @return the result of calculation
   * @throws [RuntimeException] like when divided by zero
   */
  @Throws(RuntimeException::class)
  fun apply(leftOperand: Float, rightOperand: Float): Float


}

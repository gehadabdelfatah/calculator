package android.goda.calculator.repository


import kotlinx.coroutines.flow.Flow
import android.goda.calculator.data.local.db.EquationDAO
import android.goda.calculator.data.local.db.EquationEntity
import javax.inject.Inject

class RepositoryImpl @Inject constructor(private val equationsDAO: EquationDAO) : CalculatorRepository {


    override suspend fun insertEquation(equation: EquationEntity): Long = equationsDAO.insertEquation(equation)
    override suspend fun updateEquation(id: Long) = equationsDAO.updateEquation(id)
    override fun getAllEquations(): Flow<List<EquationEntity>> = equationsDAO.getAllEquations()

}
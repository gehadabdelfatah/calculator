package android.goda.calculator.repository

import kotlinx.coroutines.flow.Flow
import android.goda.calculator.data.local.db.EquationEntity

interface CalculatorRepository {

    suspend fun insertEquation(equation: EquationEntity) : Long
    suspend fun updateEquation(id: Long)
     fun getAllEquations(): Flow<List<EquationEntity>>

}
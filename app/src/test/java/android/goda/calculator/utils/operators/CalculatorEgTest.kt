package android.goda.calculator.utils.operators

import com.google.common.truth.Truth.assertThat
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class CalculatorEgTest {

    private val cE = CalculatorEg()

    @Before
    fun setup() {
        cE.clear()
    }

    @Test
    fun add() {
        Assert.assertEquals(4, (2 + 2).toLong())
    }
    @Test
    fun `testForSub1`() {
        cE.operator = "-"
        val result =  cE.calculate(5f,1f)
        assertThat(result).isEqualTo(4f)
    }
    @Test
    fun `testForAddition`() {
        cE.operator = "+"
        val result =  cE.calculate(3f,6f)
        assertThat(result).isEqualTo(9f)
    }

    @Test
    fun `testForSub2`() {
        cE.operator = "-"
        val result =  cE.calculate(-1f,1f)
        assertThat(result).isEqualTo(-2f)
    }

    @Test
    fun `testForMult`() {

        cE.operator = "*"
        val result =  cE.calculate(6f,10f)
        assertThat(result).isEqualTo(60f)
        Assert.assertEquals(
            4, (2 * 2))
    }

    @Test
    fun mul() {
        Assert.assertEquals(4, (2 * 2).toLong())
    }
    @Test
    fun `testForDivision`() {
        cE.operator = "/"
        val result =  cE.calculate(10f,2f)
        assertThat(result).isEqualTo(5f)

    }
}
package android.goda.calculator.presentation.activity

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.google.common.truth.Truth.assertThat
import android.goda.calculator.utils.MainCoroutineRule
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.newSingleThreadContext
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import android.goda.calculator.data.local.db.EquationEntity
import android.goda.calculator.getOrAwaitValueTest
import android.goda.calculator.presentation.viewmodel.CalculatorViewModel
import android.goda.calculator.utils.operators.CalculatorEg
import android.goda.calculator.utils.Constant
import android.goda.calculator.repository.FakeRepo
import android.goda.calculator.utils.InstantExecutorExtension
import org.junit.After
import org.junit.Rule
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.Mock


@ExperimentalCoroutinesApi
@ExtendWith(InstantExecutorExtension::class)
class CalculatorViewModelTest {

    @get:Rule
    var instantTaskExecutorRule =  InstantTaskExecutorRule()
    // Set the main coroutines dispatcher for unit testing.
    @ExperimentalCoroutinesApi
    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()




    @Mock
    private lateinit var calculatorViewModel: CalculatorViewModel

    private val mainThreadSurrogate = newSingleThreadContext("Main thread")

    @BeforeEach
    fun setup() {
        calculatorViewModel = CalculatorViewModel(FakeRepo(), CalculatorEg())
        Dispatchers.setMain(mainThreadSurrogate)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain() // reset main dispatcher to the original Main dispatcher
        mainThreadSurrogate.close()
    }

    @Test
    fun `test get all equations, returns List`(){

        runBlockingTest {
            val equationEntity = EquationEntity(1f, 1f, "+", 2f,  true)
            equationEntity.id = 1

            calculatorViewModel.insertEquation(equationEntity)

            calculatorViewModel.getAllEquations()

            val result = calculatorViewModel.allEquationLiveData.getOrAwaitValueTest()
            assertThat(result.data).isNotEmpty()
        }

    }
    @Test
    fun `test calculation when num 1 equal null return error msg` () {
        val result =  calculatorViewModel.doCalculation(num1 = null, num2 = 1f, operator = "+")
        assertThat(result.errorMassage).isEqualTo(Constant.NUM_1_NULL_MSG)
    }

    @Test
    fun `test calculation when num 2 equal null return error msg` () {
        val result =  calculatorViewModel.doCalculation(num1 = 1f, num2 = null, operator = "+")
        assertThat(result.errorMassage).isEqualTo(Constant.NUM_2_NULL_MSG)
    }

    @Test
    fun `test calculation when num 1 equal 0 and operator equal div return error msg` () {
        val result =  calculatorViewModel.doCalculation(num1 = 0f, num2 = 1f, operator = "/")
        assertThat(result.errorMassage).isEqualTo(Constant.CANT_DIVIDE)
    }

    @Test
    fun `test calculation when operator equal null return error msg` () {
        val result =  calculatorViewModel.doCalculation(num1 = 1f, num2 = 1f, operator = "")
        assertThat(result.errorMassage).isEqualTo(Constant.OPERATOR_NULL_OR_EMPTY_MSG)
    }

    @Test
    fun `test insert equation with valid input, returns valid id`(){
        val insertedID = 1
        val equationEntity = EquationEntity(1f, 1f, "+", 2f,  true)
        equationEntity.id = 1

        calculatorViewModel.insertEquation(equationEntity)

        val result = calculatorViewModel.insertedID.getOrAwaitValueTest()
        assertThat(result.getContentIfNotHandled()?.toInt()).isEqualTo(insertedID)
    }





}
package android.goda.calculator.data.local.dp

import android.content.Context
import android.goda.calculator.data.local.db.EquationDAO
import android.goda.calculator.data.local.db.EquationEntity
import android.goda.calculator.data.local.db.EquationsDatabase
import androidx.lifecycle.LiveData
import androidx.test.core.app.ApplicationProvider
import androidx.test.runner.AndroidJUnit4
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import java.util.concurrent.Flow

@RunWith(AndroidJUnit4::class)
class DatabaseTest {

    var equationDao: EquationDAO? = null

    @Mock
    var pendingEquationsMock: LiveData<List<EquationEntity>>? = null


    @Mock
    var solvedEquationsMock: LiveData<List<EquationEntity>>? = null



}